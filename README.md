# DTUHAX website

## Running it locally
This website is all static pages, so running it locally is simple
```bash
cd site
python -m http.server
```

## Deploying

Any commits to the `main` branch will be deployed to [hackerlab.dtu.dk](https://hackerlab.dtu.dk/)
