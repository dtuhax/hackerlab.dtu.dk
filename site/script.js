function copyFunction() {
    var text = "To: mcla@dtu.dk\nSubject: Card access to hackerlab\n\nI hereby agree to the terms and conditions of the hackerlab, as per the following documents:\n  - DTU Compute Guidelines\n    - https://hackerlab.dtu.dk/contracts/20170815_DTUCompute_Guidelines_approved.pdf\n    - sha256sum 08a818764a9d745094107154b948d1b6a03a2783e59585b3e63b0cbfb14d916f\n  - DTU Hackerlab Guidelines\n    - https://hackerlab.dtu.dk/contracts/Kontrakt_eng.pdf\n    - sha256sum eaf07372e4635cafcb1bbd407ba414795d51ce1c24206993887ad6c4ec48e9c7\nCan you give me card access to hackerlab?\n\nThanks.\n";
    navigator.clipboard.writeText(text);
    window.getSelection().removeAllRanges();
}